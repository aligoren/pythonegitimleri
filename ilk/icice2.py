print('Programdann cikmak icin [c] harfine basiniz: ')
while True:
    i = input('Katekoku alinacak sayiyi giriniz: ')
    if i == 'c':
        print ('[c] tusuna bastiniz. programdan cikis yapiliyor')
        break
    if int(i) < 0:
        print('Karmasik bir sayi 0 sayisindan kucuk. Hesaplarken zorlandim')
        continue
    k = int(i)**(1/2)
    print('Girdiginiz sayisinin karekoku: ', k)