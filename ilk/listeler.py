ogrenciler = []

ogrenciler.append('Ali')
ogrenciler.append('Erkan')
ogrenciler.append('Onur')
ogrenciler.append('Ayse')

print(ogrenciler)

print(ogrenciler[1])

ogrenciler[1] = 'Mustafa'

print(ogrenciler[1])

print(len(ogrenciler))

araclar = ['Renault', 'Mazda', 'Tofas']
araclar.append('Porsche')

print(araclar)

araclar[-1] = 'Mercedes'

print(araclar)

elemanSayisi = len(araclar)

i = 0
while i < elemanSayisi:
    print(araclar[i])
    i = i + 1

